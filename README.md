# Early Transcaucasian Phenomenon in Structural-Systemic Perspective: Map Repository

The PDF maps in this repository are the original high-resolution versions used in the following journal article:-

- Wilkinson, T. C. 2014. The Early Transcaucasian Phenomenon in Structural-Systemic Perspective: Cuisine, Craft and Economy. _Paléorient_ 40 (2): 203-229. 

See http://tobywilkinson.co.uk/publications for more information and link to the article itself.


## Licence

The maps are released under a CC BY-NC-SA 4.0 policy.